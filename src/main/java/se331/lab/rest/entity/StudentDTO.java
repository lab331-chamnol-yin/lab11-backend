package se331.lab.rest.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StudentDTO {

    Long id;
    String studentId;
    String name;
    String surname;
    Double gpa;
    String image;
    boolean featured;
    Integer penAmount;

    public Student getStudent() {
        return Student.builder()
                .id(this.id)
                .studentId(this.studentId)
                .name(this.name)
                .surname(this.surname)
                .gpa(this.gpa)
                .image(this.image)
                .penAmount(this.penAmount)
                .build();
    }
}
