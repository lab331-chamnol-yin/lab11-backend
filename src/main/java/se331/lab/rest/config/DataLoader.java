package se331.lab.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se331.lab.rest.entity.Course;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.entity.Student;
import se331.lab.rest.repository.CourseRepository;
import se331.lab.rest.repository.LecturerRepository;
import se331.lab.rest.repository.StudentRepository;

import java.util.ArrayList;
import java.util.HashSet;

@Component
public class DataLoader implements ApplicationRunner {
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    LecturerRepository lecturerRepository;
    @Autowired
    CourseRepository courseRepository;
    @Override
    @Transactional
    public void run(ApplicationArguments args) throws Exception {
        Student student1 = Student.builder()
                .studentId("SE-001")
                .name("Prayuth")
                .surname("The minister")
                .gpa(3.59)
                .image("http://13.250.41.39:8190/images/tu.jpg")
                .penAmount(15)
                .description("The great man ever!!!!")
                .build();
        Student student2 = Student.builder()
                .studentId("SE-002")
                .name("Cherprang")
                .surname("BNK48")
                .gpa(4.01)
                .image("http://13.250.41.39:8190/images/cherprang.png")
                .penAmount(2)
                .description("Code for Thailand")
                .build();
        Student student3 = Student.builder()
                .studentId("SE-003")
                .name("Nobi")
                .surname("Nobita")
                .gpa(1.77)
                .image("http://13.250.41.39:8190/images/nobita.gif")
                .penAmount(0)
                .description("Welcome to Olympic")
                .build();
        Student student4 = Student.builder()
                .studentId("SE-004")
                .name("Jurgen")
                .surname("Klopp")
                .gpa(2.56)
                .image("http://13.250.41.39:8190/images/klopp.jpg")
                .penAmount(200)
                .description("The manager")
                .build();
        Student student5 = Student.builder()
                .studentId("SE-005")
                .name("Mohamed")
                .surname("Salah")
                .gpa(2.44)
                .image("http://13.250.41.39:8190/images/salaaa.jpg")
                .penAmount(0)
                .description("The King of Egypt")
                .build();

        this.studentRepository.save(student1);
        this.studentRepository.save(student2);
        this.studentRepository.save(student3);
        this.studentRepository.save(student4);
        this.studentRepository.save(student5);

        Lecturer lecturer1 = Lecturer.builder()
                .name("Chartchai")
                .surname("Doungsa-ard")
                .build();
        Lecturer lecturer2 = Lecturer.builder()
                .name("Jayakrit")
                .surname("Hirisajja")
                .build();
        Lecturer lecturer3 = Lecturer.builder()
                .name("Pree")
                .surname("Thiengburanathum")
                .build();
        this.lecturerRepository.save(lecturer1);
        this.lecturerRepository.save(lecturer2);
        this.lecturerRepository.save(lecturer3);
        Course course1 = Course.builder()
                .courseId("953331")
                .courseName("Component Based Software Dev")
                .content("Nothing just for fun")
                .build();
        Course course2 = Course.builder()
                .courseId("953xxx")
                .courseName("X project")
                .content("Do not know what to study")
                .build();
        Course course3 = Course.builder()
                .courseId("953494")
                .courseName("Selected Topics in SE 1")
                .content("Python and Data mining Development")
                .build();
        Course course4 = Course.builder()
                .courseId("953234")
                .courseName("Advance Software Development")
                .content("The course for the smart students ")
                .build();
        this.courseRepository.save(course1);
        this.courseRepository.save(course2);
        this.courseRepository.save(course3);
        this.courseRepository.save(course4);
        lecturer1.getAdvisees().add(student1);
        student1.setAdvisor(lecturer1);
        lecturer1.getAdvisees().add(student2);
        student2.setAdvisor(lecturer1);
        lecturer2.getAdvisees().add(student3);
        student3.setAdvisor(lecturer2);
        lecturer3.getAdvisees().add(student4);
        student4.setAdvisor(lecturer3);
        lecturer3.getAdvisees().add(student5);
        student5.setAdvisor(lecturer3);

        student1.getEnrolledCourses().add(course1);
        course1.getStudents().add(student1);
        student1.getEnrolledCourses().add(course2);
        course2.getStudents().add(student1);
        student2.getEnrolledCourses().add(course1);
        course1.getStudents().add(student2);
        student3.getEnrolledCourses().add(course2);
        course2.getStudents().add(student3);
        course3.getStudents().add(student4);
        course3.getStudents().add(student5);
        course3.getStudents().add(student1);
        student1.getEnrolledCourses().add(course3);
        student4.getEnrolledCourses().add(course3);
        student5.getEnrolledCourses().add(course3);
        course4.getStudents().add(student1);
        course4.getStudents().add(student2);
        course4.getStudents().add(student3);
        course4.getStudents().add(student4);
        course4.getStudents().add(student5);
        student1.getEnrolledCourses().add(course4);
        student2.getEnrolledCourses().add(course4);
        student3.getEnrolledCourses().add(course4);
        student4.getEnrolledCourses().add(course4);
        student5.getEnrolledCourses().add(course4);


        course1.setLecturer(lecturer2);
        lecturer2.getCourses().add(course1);
        course2.setLecturer(lecturer1);
        lecturer1.getCourses().add(course2);
        course3.setLecturer(lecturer3);
        lecturer3.getCourses().add(course3);
        course4.setLecturer(lecturer1);
        lecturer1.getCourses().add(course4);

    }


}
